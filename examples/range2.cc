#include <cstdio>

#include "lines_of_file.h"
#include "parser/csv.h"
#include "ranges.h"

using namespace parser;

struct line_range {
  line_range(char const* path) : f_(path) {}

  auto begin() { return f_.read(); }

  template <typename It>
  auto&& read(It& it) {
    return it;
  }

  template <typename It>
  void next(It& it) {
    it = f_.read();
  }

  template <typename It>
  bool valid(It& it) {
    return it;
  }

  lines_of_file f_;
};

#include <array>

template <char Separator = ','>
std::array<column_idx_t, MAX_COLUMNS> read_header(
    cstr s, std::initializer_list<cstr> defined_columns) {
  std::array<column_idx_t, MAX_COLUMNS> column_map;
  column_map.fill(NO_COLUMN_IDX);

  for (column_idx_t column = 0; s; ++column) {
    cstr header;
    parse_column<cstr, Separator>(s, header);

    column_map[column] = NO_COLUMN_IDX;
    auto c = 0u;
    for (auto const& defined_column : defined_columns) {
      if (header == defined_column) {
        column_map[column] = c;
        break;
      }
      ++c;
    }

    if (s) {
      ++s;
    }
  }

  return column_map;
}

template <typename LineRange>
struct csv_range : public LineRange {
  csv_range(LineRange&& r, std::initializer_list<cstr>&& headers)
      : LineRange(std::move(r)),
        headers_permutation_{read_header(LineRange::begin(), headers)} {}

  template <char Separator = ','>
  inline std::array<cstr, MAX_COLUMNS> read_row(cstr s) {
    std::array<cstr, MAX_COLUMNS> row;
    for (column_idx_t i = 0; i < MAX_COLUMNS && s; ++i) {
      cstr column_content;
      parse_column<cstr, Separator>(s, column_content);

      if (headers_permutation_[i] != NO_COLUMN_IDX) {
        row[headers_permutation_[i]] = column_content;
      }

      if (s) {
        ++s;
      }
    }
    return row;
  }

  auto begin() { return read_row(LineRange::begin()); }

  template <typename It>
  auto&& read(It& it) {
    return it;
  }

  template <typename It>
  void next(It& it) {
    cstr s;
    LineRange::next(s);
    it = read_row(s);
  }

  template <typename It>
  bool valid(It& it) {
    return it[0];
  }

  std::initializer_list<cstr> headers_;
  std::array<column_idx_t, MAX_COLUMNS> headers_permutation_;
};

struct csv_t {
  csv_t(std::initializer_list<cstr>&& headers) : headers_(std::move(headers)) {}

  template <typename LineRange>
  friend auto operator|(LineRange&& r, csv_t&& t) {
    return csv_range<LineRange>{std::forward<LineRange>(r),
                                std::move(t.headers_)};
  }

  std::initializer_list<cstr> headers_;
};

auto csv(std::initializer_list<cstr>&& headers) {
  return csv_t{std::move(headers)};
}

constexpr auto const path = "/Users/felix/Downloads/IBM_adjusted_header.txt";

int main() {
  auto r = line_range{path}  //
           | csv({"date", "time", "open", "high", "low", "close", "volume"})  //
           | transform([](auto&& row) { return parse<int>(row[6]); })  //
           | accumulate(
                 [](auto&& acc, auto&& i) {
                   return std::make_pair(acc.first + i, acc.second + 1);
                 },
                 std::make_pair(0ULL, 0ULL));
  printf("%llu\n", r.second != 0 ? r.first / r.second : 0);
}